import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import * as Sentry from "@sentry/vue";

import './assets/main.css'

const app = createApp(App)

Sentry.init({
    app,
    dsn: "https://ffd4d5a593474c45a2f0e36e60f8cef6@app.glitchtip.com/5303",
    integrations: [
        new Sentry.BrowserTracing({
                routingInstrumentation: Sentry.vueRouterInstrumentation(router),
            }),
            new Sentry.Replay(),
        ],
    environment: import.meta.env.VITE_ENVIRONMENT_TYPE,
    tracesSampleRate: 1.0,
    replaysSessionSampleRate: 0.1,
    replaysOnErrorSampleRate: 1.0,
    });

app.use(router).mount('#app')
