import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'Główna',
      component: Home
    },
    {
      path: '/signin',
      name: 'Rejestracja',
      component: () => import('../views/Authorization.vue'),
      meta: {
        title: "rejestracja - konwenty.moe"
      }
    },
    {
      path: '/konwent/:conId',
      name: 'Konwent',
      component: () => import('../views/Convention.vue')
    },
    {
      path: '/profil',
      name: 'Profil',
      component: () => import('../views/Profile.vue'),
      meta: {
        title: "twój profil - konwenty.moe"
      }
    },
    {
      path: '/kalendarz',
      name: 'Kalendarz konwentów',
      component: () => import('../views/Calendar.vue'),
      meta: {
        title: "kalendarz konwentów - konwenty.moe"
      }
    },
    {
      path: '/szukajka',
      name: 'Wyszukiwarka konwentów',
      component: () => import('../views/Search.vue'),
      meta: {
        title: "wyszukiwarka konwentow - konwenty.moe"
      }
    }
  ]
})

export default router
