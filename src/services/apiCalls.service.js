const baseURL = import.meta.env.VITE_API_BASEURL;
const headers = {
  'Content-Type' : 'application/json'
}

class apiCalls {
  async get(path, parseJSON = true) {
    return fetch( baseURL + path, { headers: headers } )
      .then(res => {
        if (res.ok){
          return parseJSON == true ? res.json() : res
        } else {
          throw res.status;
        }
      })
    }
    async post(path, body, parseJSON = true) {
      return fetch( baseURL + path, { headers: headers, method: 'POST', body: body })
      .then(res => {
        if (res.ok) {
          return parseJSON == true ? res.json() : res
        } else {
          throw res.status;
        }
      })
  }
}

export default new apiCalls();
