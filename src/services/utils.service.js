const defaultDateFormat = {day: '2-digit', month: "long", year: "numeric"}

class utils {
    debounce(func, wait = 300){
        let timer;
        return function(...args){
            if(timer) {
                clearTimeout(timer);
            }
            const context = this;
            timer = setTimeout(()=>{
                func.apply(context, args);
            }, wait);
        }
    }
    parseDate(dateString, dateFormat = defaultDateFormat){
        const date = new Date(dateString);
        return new Intl.DateTimeFormat('pl-PL', dateFormat).format(date);
    }
}

export default new utils();
